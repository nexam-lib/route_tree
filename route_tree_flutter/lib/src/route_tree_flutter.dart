import 'package:flutter/widgets.dart';
import 'package:route_tree/route_tree.dart';

/// Function that creates a [PageRoute<T>] from [widget] and [settings]. Used by
/// [makeRouteFactory].
typedef PageBuilder<T> = PageRoute<T> Function(
    Widget widget, RouteSettings settings);

/// Creates a [RouteFactory] to pass to [WidgetsApp.onGenerateRoute].
///
/// The [routeTree] defines which path should be mapped to which [Widget].
///
/// [pageRouteBuilder] is used to transform a [Widget] into a [PageRoute].
///
/// [createAnonymousRoute] only gets called when  [RouteSettings.name] is null,
/// which is to say that the route is anonymous. When aiming to consistently use
/// named routes this can usually be the same as the widget returned by your
/// [RootSegment.createError].
///
/// For example a trivial usage of this function may look like this:
/// ```dart
/// Widget build(BuildContext context) => MaterialApp(
///   ...
///   initialRoute: "/",
///   onGenerateRoute: makeRouteFactory<dynamic>(
///     transform: (widget, settings) => MaterialPageRoute<dynamic>(
///       builder: (context) => widget,
///       settings: settings,
///     ),
///     createAnonymousRoute: () => const NoScreenFoundScreen(uri: null),
///     routeTree: Segment.root<Widget>(
///       create: (context) => const HomeScreen(),
///       createError: (context) => NoScreenFoundScreen(uri: context.uri),
///       children: [
///         ...
///       ],
///     ),
///   ),
/// ...
/// );
/// ```
RouteFactory makeRouteFactory<T>({
  required PageBuilder pageRouteBuilder,
  required RootSegment<Widget> routeTree,
  required Widget Function() createAnonymousRoute,
}) =>
    (RouteSettings settings) {
      final name = settings.name;
      return name != null
          ? pageRouteBuilder(routeTree.routeRaw(name), settings)
          : pageRouteBuilder(createAnonymousRoute(), settings);
    };
