# route_tree_flutter

Simplifies using route_tree with Flutter.

Provides the function [makeRouteFactory] to simplify using route_tree for routing in a Flutter application, without adding a dependency to the main package.
