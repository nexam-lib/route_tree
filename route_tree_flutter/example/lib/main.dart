import 'package:flutter/material.dart';
import 'package:route_tree/parser.dart';
import 'package:route_tree/route_tree.dart';
import 'package:route_tree_flutter/route_tree_flutter.dart';

void main() => runApp(FlutterRouteTreeExampleApp());

class FlutterRouteTreeExampleApp extends StatelessWidget {
  Widget build(BuildContext context) => MaterialApp(
    title: 'route_tree demo',
    theme: ThemeData(primarySwatch: Colors.blue),
    initialRoute: "/",
    onGenerateRoute: makeRouteFactory<dynamic>(
      pageRouteBuilder: (widget, settings) => MaterialPageRoute<dynamic>(
        builder: (context) => widget,
        settings: settings,
      ),
      createAnonymousRoute: () => const NoScreenFoundScreen(),
      routeTree: Segment.root<Widget>(
        create: (context) => const HomeScreen(),
        createError: (context) => NoScreenFoundScreen(uri: context.uri),
        children: [
          Segment.path(
            name: "users",
            create: (context) => const ListUsersScreen(),
            children: [
              Segment.param(
                parser: const UintParser("userId"),
                create: (context) => UserDetailScreen(id: context["userId"] as int),
              ),
            ],
          ),
        ],
      ),
    ),
  );
}

class HomeScreen extends StatelessWidget {
  const HomeScreen();

  Widget build(BuildContext context) => Scaffold(
    body: ListView(
      children: [
        ListTile(
          title: const Text("List of user ids"),
          onTap: () => Navigator.of(context).pushNamed("/users"),
        ),
      ],
    ),
  );
}

class ListUsersScreen extends StatelessWidget {
  const ListUsersScreen();

  Widget build(BuildContext context) => Scaffold(
    body: ListView.builder(
      itemBuilder: (context, i) => ListTile(
        title: Text("User $i"),
        onTap: () => Navigator.of(context).pushNamed("/users/$i"),
      ),
    ),
  );
}

class UserDetailScreen extends StatelessWidget {
  const UserDetailScreen({
    required this.id,
  });

  final int id;

  Widget build(BuildContext context) => Scaffold(
    body: Center(
      child: Text("User Screen for id $id"),
    ),
  );
}

class NoScreenFoundScreen extends StatelessWidget {
  const NoScreenFoundScreen({
    this.uri,
  });

  final Uri? uri;

  Widget build(BuildContext context) => Scaffold(
    body: Center(
      child: Text("No Screen found for route $uri"),
    ),
  );
}
