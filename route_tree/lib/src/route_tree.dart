import 'package:result_class/result_class.dart';

import 'segment_parser.dart';
import 'validation_error.dart';
import 'verifiers.dart';

/// A function that gets called if its owning [Segment] matches a path.
///
/// [T] can be Widget or some kind of RouteHandler for  as part of a backend.
typedef RouteBuilder<T> = T Function(ParseContext<T> context);

/// A function that gets called with the [RootSegment].
///
/// It should return [Result.err] with an error message if there is something
/// wrong, otherwise it should return [Result.ok(null)], which means that
/// everything is ok.
typedef SegmentVerifier<T> = Result<void, String> Function(
    Segment<T> current, String path);

/// Represents a single node in a route tree.
///
/// [T] is the type of objects that get returned on routing to a path.
/// It could be Widget or some kind of RouteHandler
abstract class Segment<T> {
  /// {@template route_tree.segment.root}
  /// Creates a [RootSegment]
  ///
  /// As the name implies, this should always be the root node of your route
  /// tree.
  ///
  /// Note that the constructor of [RootSegment] calls [RootSegment.verify] in
  /// its body. If you don't want that you have to manually pass an empty list
  /// to [verifiers].
  ///
  /// Use [RootSegment.route] to to the actual routing.
  ///
  /// Note that [Segment]s with [LiteralParser]s (in this library [Segment.path]
  /// which forwards to [PathSegment()]) are always looked up before any other
  /// [Segment]s, regardless of the order in which they have been defined.
  ///
  /// Internally [Segment] has two kinds of children, a
  /// [Map<String, Segment<T>>] of literal children and a [List<Segment<T>>] of
  /// non literal children, determined by whether a child has a [LiteralParser]
  /// which is usually only the case for [Segment.path] (which forwards to
  /// [PathSegment]). When routing the implementation first looks up the current
  /// path segment in the literal children, which is a O(1) operation. Only when
  /// no literal match was found does it linearly try the remaining children
  /// with an average complexity of O(n/2) for n non literal children.
  ///
  /// For example:
  ///
  /// ```dart
  /// final router = Segment.root<String>(
  ///   create: (context) => 'rootRoute',
  ///   createError: (context) => 'rootError',
  ///   children: [
  ///     Segment.regexPath(
  ///       parser: RegExpParser(RegExp(r'.*')),
  ///       create: (context) => 'regexRoute',
  ///     ),
  ///     Segment.path(
  ///       name: 'user',
  ///       create: (context) => 'userRoute',
  ///     )
  ///   ]
  /// );
  ///
  /// assert(router.route(Uri.parse('https://api.com/user')) == 'userRoute');
  /// ```
  /// {@endtemplate}
  static RootSegment<T> root<T>({
    RouteBuilder<T>? create,
    required RouteBuilder<T> createError,
    List<Segment<T>> children = const [],
    List<SegmentVerifier<T>> verifiers = const [findConflictingParamKeys],
  }) =>
      RootSegment<T>(
        create: create,
        createError: createError,
        children: children,
        verifiers: verifiers,
      );

  /// {@template route_tree.segment.path}
  /// Creates a path segment that matches a segment literally like /settings,
  /// /user, etc
  ///
  /// ```dart
  /// /// Matches '/settings/*' but not '/settings', since [create] is null
  /// Segment.path(
  ///   name: 'settings',
  ///   children: [
  ///     /// Matches '/settings/privacy'
  ///     Segment.path(
  ///       name: 'privacy',
  ///       create: (context) => 'privacy',
  ///     ),
  ///   ],
  /// ),
  /// ```
  /// {@endtemplate}
  factory Segment.path({
    required String name,
    RouteBuilder<T>? create,
    List<Segment<T>> children,
    RouteBuilder<T>? createError,
  }) = PathSegment<T>;

  /// {@template route_tree.segment.regexpath}
  /// Creates a [Segment] that matches a path segment against a regular
  /// expression.
  ///
  /// ```dart
  ///   /// Matches any path segment consisting of letters
  ///   Segment.regExpPath(
  ///     regExp: RegExp(r'[a-zA-Z]+'),
  ///     create: (context) => 'letter path',
  ///   ),
  /// ```
  /// See also:
  ///   * [RegExpParamSegment] or [Segment.regExpParam], for when you need the
  ///     matched value
  /// {@endtemplate}
  factory Segment.regExpPath({
    required RegExp regExp,
    RouteBuilder<T>? create,
    List<Segment<T>> children,
    RouteBuilder<T>? createError,
  }) = RegExpPathSegment<T>;

  /// {@template route_tree.segment.regexpparam}
  /// Creates a [Segment] that matches a path segment against a regular
  /// expression and adds it to the [ParseContext].
  ///
  /// ```dart
  ///   /// Matches any path segment consisting of letters
  ///     Segment.regExpParam(
  ///       parser: RegExpParamParser.forward(
  ///         key: 'name',
  ///         regExp: RegExp(r'\w+'),
  ///       ),
  ///       create: (context) => context['name'] as String,
  ///     ),
  /// ```
  ///
  /// See also:
  ///   * [RegExpPathSegment] or [Segment.regExpPath], for when you don't want
  ///     or need the actual matched value
  /// {@endtemplate}
  factory Segment.regExpParam({
    required RegExpParamParser parser,
    RouteBuilder<T>? create,
    List<Segment<T>> children,
    RouteBuilder<T>? createError,
  }) = RegExpParamSegment<T>;

  /// {@template route_tree.segment.param}
  /// Creates a [Segment] that matches a path segment using the [parser] and
  /// injects it into the [ParseContext]. Usually used with a [UintParser].
  ///
  /// ```dart
  ///   /// Matches '/users/{id}', where {id} is any non-negative integer as defined
  ///   /// by [UintParser].
  ///   Segment.param(
  ///     parser: const UintParser('id'),
  ///     create: (context) => context['id'] as String,
  ///   ),
  /// ```
  ///
  /// See also:
  ///   * [UintParser], for when you need to parse an id
  /// {@endtemplate}
  factory Segment.param({
    required ParamParser parser,
    RouteBuilder<T>? create,
    List<Segment<T>> children,
    RouteBuilder<T>? createError,
  }) = ParamSegment<T>;

  /// Public default constructor called by implementing classes
  Segment(this.parser, this.create, this.createError, List<Segment<T>> children)
      : assert(
          create != null || children.isNotEmpty,
          'Leaf segments should always define create.',
        ),
        _children = _Children<T>(children);

  /// Creates an instance of [T], called when the Segment that owns this
  /// callback is routed to.
  final RouteBuilder<T>? create;

  /// If [Segment.parse] cannot find a matching route the bottom most
  /// [createError] is called instead.
  ///
  /// ```dart
  ///   final router = Segment.root<String>(
  ///     create: (context) => 'rootRoute',
  ///     createError: (context) => 'rootError',
  ///     children: [
  ///       Segment.path(
  ///         name: 'settings',
  ///         create: (context) => 'settingsRoute',
  ///         createError: (context) => 'settingsError',
  ///         children: [
  ///           Segment.path(
  ///             name: 'privacySettings'
  ///             create: (context) => 'privacyRoute',
  ///           ),
  ///         ],
  ///       ),
  ///       Segment.path(
  ///         name: 'user',
  ///         create: (context) => 'userRoute',
  ///       ),
  ///     ],
  ///   );
  ///
  ///   assert(router.route(Uri.parse('http://test.com/noRoute')) == 'rootError');
  ///   assert(router.route(Uri.parse('http://test.com/user/noRoute')) == 'rootError');
  ///   assert(router.route(Uri.parse('http://test.com/settings/noRoute')) == 'settingsError');
  /// ```
  final RouteBuilder<T>? createError;

  final _Children<T> _children;

  Iterable<Segment<T>> get children => _children.asIterable;

  /// The parser that handles whether this [Segment] matches a route.
  final SegmentParser parser;

  /// Returns a [Result<T, T>?] where null means this doesn't match the current
  /// path segment, [Result.ok] means a match was found and [Result.err] means
  /// an error was encountered trying to parse the path segments.
  ///
  /// The right child is looked up by first looking up the current segment in
  /// all child segments that have a [LiteralParser], like [Segment.path]. Then,
  /// if no literal child was matched, the remaining children are tried
  /// literally.
  Result<T, T>? parse(ParseContext<T> context, Iterable<String> segments,
      RouteBuilder<T> createError) {
    createError = this.createError ?? createError;
    final result = parser.parse(context, segments.first);
    if (!result.matched) {
      return null;
    }

    result.applyTo(context);

    segments = segments.skip(1);
    if (segments.isEmpty) {
      final create = this.create;
      return create != null
          ? Result.ok(create(context))
          : Result.err(createError(context));
    }

    final literalResult = _children.literalChildren[segments.first];
    return literalResult != null
        ? literalResult.parse(context, segments, createError)
        : _children.nonLiteralChildren
            .map((child) => child.parse(context, segments, createError))
            .firstWhere(
              (result) => result != null,
              orElse: () => Result.err(createError(context)),
            );
  }
}

/// There should be only one [RootSegment] within a route_tree and it should be
/// the root [Segment]
class RootSegment<T> extends Segment<T> {
  /// {@macro route_tree.segment.root}
  RootSegment({
    RouteBuilder<T>? create,
    required this.createError,
    List<Segment<T>> children = const [],
    this.verifiers = const [findConflictingParamKeys],
  }) : super(const LiteralParser('/'), create, createError, children) {
    assert(() {
      final result = verify();
      result.mapErr((errors) => throw ValidationError(errors));
      return true;
    }());
  }

  /// Functions that get called by [verify] sequentially. Can be used to ensure
  /// that the route tree satisfies certain conditions.
  ///
  /// Defaults to [findConflictingParamKeys], which ensures that there are no
  /// duplicate nested parameter keys like /books/:id/edit/:id.
  final List<SegmentVerifier<T>> verifiers;

  @override
  final RouteBuilder<T> createError;

  /// Returns a [Result.ok] if all [verifiers] do, otherwise returns a
  /// [Result.err] with all errors returned by all [verifiers].
  Result<void, List<String>> verify() {
    final errors = verifiers
        .map((verifier) => verifier(this, '$parser'))
        .fold<Iterable<String>>(
            [],
            (iter, result) =>
                result.mapOrElse((_) => iter, (es) => iter.followedBy([es])));

    return errors.isEmpty
        ? const Result.ok(null)
        : Result.err(errors.toList(growable: false));
  }

  /// Returns the matching [T] for [uri] as defined with the [Segment] tree
  T route(Uri uri) {
    /// This is a compromise between providing the conventional behavior of
    /// 'route '/' means homepage' and the fact that [uri.pathSegments] doesn't
    /// actually include slashes.
    ///
    /// Manually prepending '/' to the segments means that [RootSegment] parses
    /// the 'homepage' as one would expect without the need for a special
    /// implementation of [RootSegment.parse] just for this case.
    final segments = ['/', ...uri.pathSegments];
    final context = ParseContext<T>(uri);
    final result = parse(context, segments, createError);

    return result == null
        ? createError(context)
        : result.mapOrElse(
            (value) => value,
            (error) => error,
          );
  }

  /// Identical to `route(Uri.parse(name))`
  T routeRaw(String name) => route(Uri.parse(name));
}

/// A [Segment] for literal path segments
class PathSegment<T> extends Segment<T> {
  /// {@macro route_tree.segment.path}
  PathSegment({
    required String name,
    RouteBuilder<T>? create,
    List<Segment<T>> children = const [],
    RouteBuilder<T>? createError,
  }) : super(LiteralParser(name), create, createError, children);
}

/// A [Segment] for path segments that match a regular expression
///
/// See also:
///   * [RegExpParamSegment], for when you need the matched path segment
class RegExpPathSegment<T> extends Segment<T> {
  /// {@macro route_tree.segment.regexpath}
  RegExpPathSegment({
    required RegExp regExp,
    RouteBuilder<T>? create,
    List<Segment<T>> children = const [],
    RouteBuilder<T>? createError,
  }) : super(RegExpParser(regExp), create, createError, children);
}

/// A [Segment] for path segments that should match a regular expression, where
/// you need the segment itself, too.
///
/// See also:
///   * [RegExpPathSegment] or [Segment.regExpPath], for when you don't want or
///     need the actual matched value
class RegExpParamSegment<T> extends Segment<T> {
  /// {@macro route_tree.segment.regexpparam}
  RegExpParamSegment({
    required RegExpParamParser parser,
    RouteBuilder<T>? create,
    List<Segment<T>> children = const [],
    RouteBuilder<T>? createError,
  }) : super(parser, create, createError, children);
}

/// A [Segment] for path parameters
///
/// See also:
///   * [ParamParser]
class ParamSegment<T> extends Segment<T> {
  /// {@macro route_tree.segment.param}
  ParamSegment({
    required ParamParser parser,
    RouteBuilder<T>? create,
    List<Segment<T>> children = const [],
    RouteBuilder<T>? createError,
  }) : super(parser, create, createError, children);
}

/* abstract class Verifier<T> {
  const Verifier({
    required this.f,
    required this.shortCircuit,
  });

  final bool shortCircuit;
  final Result<void, String> Function(Segment<T> segment, String path) f;
  
  Result<void, List<String>> call(Segment<T> current, String path) {
    final errors = <String>[];
    final r = f(current, path);

    if (r.isErr) {
      if (shortCircuit)
        return r.mapErr((error) => [error]);
      else
        errors.add(r.errorOrNull!);
    }

    if (current.children.isEmpty)
      return errors.isEmpty
        ? const Result.ok(null)
        : Result.err(errors);
    
    errors.addAll(
      current.children
        .map((child) => f(child, '$path/${child.parser.key}'))
        .fold(const [], (list, result) =>
          list.followedBy(result.mapOrElse((_) => [], (error) => [error])))
    );

    return errors.isEmpty
      ? const Result.ok(null)
      : Result.err(errors.toList(growable: false));
  }

  Result<void, String> verify(Segment<T> current);
} */

/// Container optimized for faster lookup of instances of [LiteralParser].
///
/// Contains a [Map<String, Segment<T>>] for Segments that have a
/// [LiteralParser] as well as a [List<Segment<T>>] for the rest that don't.
class _Children<T> {
  factory _Children(List<Segment<T>> children) {
    final literalChildren = <String, Segment<T>>{};
    final nonLiteralChildren = <Segment<T>>[];
    for (final child in children) {
      final parser = child.parser;
      if (parser is LiteralParser) {
        assert(!literalChildren.containsKey(parser.key),
            'Found duplicate segment with name \"${parser.key}\"');
        literalChildren[parser.key] = child;
      } else {
        nonLiteralChildren.add(child);
      }
    }

    return _Children._(literalChildren, nonLiteralChildren);
  }

  const _Children._(this.literalChildren, this.nonLiteralChildren);

  final Map<String, Segment<T>> literalChildren;
  final List<Segment<T>> nonLiteralChildren;

  Iterable<Segment<T>> get asIterable =>
      literalChildren.values.followedBy(nonLiteralChildren);
}
