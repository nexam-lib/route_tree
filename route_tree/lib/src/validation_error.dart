class ValidationError extends AssertionError {
  ValidationError(List<String> errors) : super(errors);

  List<String> get errors => message as List<String>;

  @override
  String toString() => errors.join('\n');
}
