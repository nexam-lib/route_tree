import 'package:result_class/result_class.dart';
import 'route_tree.dart';
import 'segment_parser.dart';

Result<void, String> findConflictingParamKeys<T>(
        Segment<T> current, String path) =>
    _findConflictingParamKeysImpl(current, path, <String, String>{})
        .mapErr((errors) => errors.join('\n'));

Result<void, List<String>> _findConflictingParamKeysImpl<T>(
    Segment<T> current, String path, Map<String, String> paramKeyPositions) {
  final parser = current.parser;

  if (parser is ParamParser) {
    final key = parser.key;
    if (paramKeyPositions.containsKey(key)) {
      return Result.err(['Found duplicate for key \"$path\".']);
    } else {
      paramKeyPositions[key] = path;
    }
  }

  if (current.children.isEmpty) {
    return const Result.ok(null);
  }

  final errors = current.children
      .map((c) => _findConflictingParamKeysImpl(c,
          '${path == '/' ? '' : path}/${c.parser}', Map.of(paramKeyPositions)))
      .fold<Iterable<String>>(
    const [],
    (list, result) =>
        list.followedBy(result.mapOrElse((_) => [], (errors) => errors)),
  );

  return errors.isEmpty
      ? const Result.ok(null)
      : Result.err(errors.toList(growable: false));
}
