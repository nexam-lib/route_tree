import 'package:route_tree/src/route_tree.dart';

/// A function tha defines a predicate by which path segments are matched
///
/// See also:
///  * [ParseResult], for what this function should return, specifically
///    * [ParseResult.success] and
///    * [ParseResult.none]
typedef Parser = ParseResult Function(ParseContext context, String source);

/// A function that defines a predicate by which path segments are matched and
/// returns a [ParseParamResult].
///
/// See also:
///  * [ParseResult], for what this function should return
///    * [ParseResult.value<T>] and
///    * [ParseResult.noValue]
typedef ValueParser = ParseParamResult Function(
    ParseContext context, String source);

/// Handles parsing single path segments a [Segment].
///
/// See also:
///  * [ParamParser], for when you need the parsed value e.g. as [int] id
abstract class SegmentParser {
  const SegmentParser();

  /// Creates a user defined [SegmentParser]
  const factory SegmentParser.withFunction(Parser parse) = _CustomSegmentParser;

  /// Handles the actual parsing logic
  ///
  /// See also:
  ///  * [ParseResult], for what the different types of [ParseResult] mean
  ParseResult parse(ParseContext context, String source);
}

class _CustomSegmentParser extends SegmentParser {
  const _CustomSegmentParser(this.parser);

  final Parser parser;

  @override
  ParseResult parse(ParseContext context, String source) =>
      parser(context, source);
}

/// [SegmentParser] that matches a [String] literally with [String.operator==].
class LiteralParser extends SegmentParser {
  const LiteralParser(this.key);

  final String key;

  @override
  ParseResult parse(ParseContext context, String source) =>
      source == key ? ParseResult.success : ParseResult.none;

  @override
  String toString() => key;
}

/// [SegmentParser] that matches a segment against the supplied [regExp].
class RegExpParser extends SegmentParser {
  const RegExpParser(this.regExp);

  /// The regular expression a segment is matched against
  final RegExp regExp;

  @override
  ParseResult parse(ParseContext context, String source) =>
      regExp.hasMatch(source) ? ParseResult.success : ParseResult.none;

  @override
  String toString() => '{${regExp.pattern}}';
}

/// Base class for parsers that inject a key-value pair into the [ParseContext]
abstract class ParamParser<T> extends SegmentParser {
  const ParamParser(this.key);

  /// Creates a user define [ParamParser]
  const factory ParamParser.withFunction({
    required String key,
    required ValueParser parser,
  }) = _CustomParamParser;

  /// The key by which the injected value of a parameter can be exctracted from
  /// [ParseContext].
  ///
  /// Keys that are equal to one of the ascendants' or descendants' keys are not
  /// allowed.
  final String key;

  @override
  ParseParamResult parse(ParseContext context, String source);

  @override
  String toString() => '{$key}';
}

class _CustomParamParser<T> extends ParamParser<T> {
  const _CustomParamParser({
    required String key,
    required this.parser,
  }) : super(key);

  final ValueParser parser;

  @override
  ParseParamResult parse(ParseContext context, String source) =>
      parser(context, source);
}

/// [SegmentParser] that matches a segment against the supplied [regExp], like
/// [RegExpParser], except that it also injects the resulting key-value pair
/// into the [ParseContext], which can be queried from [Segment.create] or
/// [Segment.createError].
class RegExpParamParser<T> extends ParamParser<T> {
  RegExpParamParser({
    required String key,
    required this.regExp,
    required this.map,
  }) : super(key);

  /// Creates a [RegexParamParser<String>] directly using the identity function
  /// as [map]
  static RegExpParamParser<String> forward({
    required String key,
    required RegExp regExp,
  }) =>
      RegExpParamParser(
        key: key,
        regExp: regExp,
        map: _identity,
      );

  static String _identity(RegExpMatch match) => match.input;

  /// The regular expression a segment is matched against
  final RegExp regExp;

  /// User defined conversion from a [RegExpMatch] to [T]. To include the
  /// matched [String] literally, use the static factory method
  /// [RegExpParamParser.forward] instead.
  final T Function(RegExpMatch match) map;

  @override
  ParseParamResult parse(ParseContext context, String source) {
    final match = regExp.firstMatch(source);

    return match != null
        ? ParseResult.value(key, map(match))
        : ParseResult.noValue;
  }

  @override
  String toString() => '{$key:${regExp.pattern}}';
}

/// A [SegmentParser] that matches an integer and injects it into the
/// [ParseContext]
class IntParser extends ParamParser<int> {
  const IntParser(String key) : super(key);

  @override
  ParseParamResult parse(ParseContext context, String source) {
    final i = int.tryParse(source);
    return i != null ? ParseResult.value(key, i) : ParseResult.noValue;
  }
}

/// A [SegmentParser] that matches an unsigned integer (like an id) and injects
/// it into the [ParseContext]
class UintParser extends ParamParser<int> {
  const UintParser(String key) : super(key);

  @override
  ParseParamResult parse(ParseContext context, String source) {
    final i = int.tryParse(source);
    return i != null && i >= 0
        ? ParseResult.value(key, i)
        : ParseResult.noValue;
  }
}

/// The parse state consisting of the [uri] and parameters injected into it
///
/// Parameters can be queried with [operator[]]
class ParseContext<T> {
  ParseContext(this.uri) : _parameters = <String, dynamic>{};

  /// The [Uri] to route to
  final Uri uri;

  /// Delegates to [Uri.queryParameters]
  ///
  /// Use [operator[]] to access single parameters directly
  Map<String, dynamic> get queryParameters => uri.queryParameters;

  /// Delegates to [Uri.queryParametersAll]
  ///
  /// Use [operator[]] to access single parameters directly
  Map<String, dynamic> get queryParametersAll => uri.queryParametersAll;

  /// Returns the value for the given [key] or null if [key] is not in the map
  dynamic operator [](String key) => _parameters[key];

  /// Returns a view of the parameters as an unmodifiable map
  Map<String, dynamic> asUnmodifiableMap() =>
      Map<String, dynamic>.unmodifiable(_parameters);

  final Map<String, dynamic> _parameters;

  @override
  String toString() {
    final parameters = () {
      if (_parameters.isEmpty) {
        return '{},';
      }

      final buffer = StringBuffer('{\n')
        ..writeAll(
            _parameters.entries.map<String>((e) => '${e.key}: ${e.value},\n'))
        ..write('},');
      return '$buffer';
    }();
    return 'ParseContext(\n  uri: $uri,\n  parameters: $parameters\n)';
  }
}

/// The result of a parse operation, where
///   * [ParseResult.none] means the input did not satisfy the parsers predicate
///   * [ParseResult.success] means the parses succeeded without a parameter and
///   * [ParseResult.value<T>] means the parser succeeded with a T
///   * [ParseResult.noValue] means the parser failed where a parameter was
///     expected
///
/// Note that there is no `ParseResult.error` or such, as a parser failing to
/// match a segment just means that the next one is tried.
abstract class ParseResult {
  /// ParseResult representing no match
  static const ParseResult none = _NoResult();

  /// [ParseResult] representing a successful match that is not a parameter
  static const ParseResult success = _ParseSuccess();

  /// [ParseResult] representing a successful match that injects a parameter
  /// into the [ParseContext] that can be queried with
  /// [ParseContext.operator[]].
  static ParseParamResult value<T>(String key, T value) =>
      _ParseSuccessWithParams(key, value);

  /// [ParseResult] representing no match when otherwise a value was expected
  static const ParseParamResult noValue = _ParseFailureWithParams();

  const ParseResult._();

  /// Whether this is a successful match
  bool get matched;

  void applyTo(ParseContext<dynamic> context) {}

  @override
  String toString() => '$runtimeType';
}

class _NoResult extends ParseResult {
  const _NoResult() : super._();

  @override
  bool get matched => false;
}

class _ParseSuccess extends ParseResult {
  const _ParseSuccess() : super._();

  @override
  bool get matched => true;
}

/// Marker interface for [ParseResult]s that should get injected into a
/// [ParseContext]
abstract class ParseParamResult extends ParseResult {
  const ParseParamResult._() : super._();
}

class _ParseSuccessWithParams<T> extends ParseParamResult {
  const _ParseSuccessWithParams(this.key, this.value) : super._();

  final String key;
  final T value;

  @override
  bool get matched => true;

  @override
  void applyTo(ParseContext context) => context._parameters[key] = value;
}

class _ParseFailureWithParams extends ParseParamResult {
  const _ParseFailureWithParams() : super._();

  @override
  bool get matched => false;
}
