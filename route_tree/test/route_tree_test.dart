import 'package:collection/collection.dart';
import 'package:route_tree/src/route_tree.dart';
import 'package:route_tree/src/segment_parser.dart';
import 'package:route_tree/src/validation_error.dart';
import 'package:test/test.dart';

class Res {
  const Res(this.id, this.parameters);

  static Res Function(ParseContext) builder(Symbol id) =>
      (context) => Res(id, context.asUnmodifiableMap());

  final Symbol id;
  final Map<String, dynamic> parameters;

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
    final mapEquals = const DeepCollectionEquality().equals;

    return o is Res && o.id == id && mapEquals(o.parameters, parameters);
  }

  @override
  int get hashCode => id.hashCode ^ parameters.hashCode;

  @override
  String toString() => 'Res(id: $id, parameters: $parameters)';
}

void main() {
  group('Segment', () {
    final router = Segment.root<Res>(
      create: Res.builder(#root),
      createError: Res.builder(#rootError),
      children: [
        Segment.path(
          name: 'anna',
          create: Res.builder(#anna),
        ),
        Segment.path(
          name: 'bernd',
          children: [
            Segment.param(
              parser: const IntParser('berndId'),
              create: Res.builder(#berndId),
              children: [
                Segment.path(
                  name: 'charlotte',
                  create: Res.builder(#charlotte),
                ),
              ],
            ),
          ],
        ),
        Segment.path(
            name: 'multipleParams',
            create: Res.builder(#multipleParams),
            createError: Res.builder(#multipleParamsError),
            children: [
              Segment.param(
                parser: const UintParser('multipleUintId'),
                create: Res.builder(#multipleUintId),
              ),
              Segment.regExpParam(
                parser: RegExpParamParser.forward(
                  key: 'regexParam',
                  regExp: RegExp(r'^[a-zA-Z]{3}$'),
                ),
                create: Res.builder(#regexParam),
              )
            ])
      ],
    );

    final base = Uri.parse('http://router.test');
    Uri path(String path) => base.replace(path: path);
    const emptyMap = <String, dynamic>{};
    test('#routing', () {
      expect(router.route(base), const Res(#root, emptyMap));
      expect(router.route(path('anna')), const Res(#anna, emptyMap));
      expect(router.route(path('bernd')), const Res(#rootError, emptyMap));

      expect(router.route(path('anna/bernd')), const Res(#rootError, emptyMap));
      expect(
        router.route(path('bernd/12')),
        const Res(#berndId, <String, dynamic>{'berndId': 12}),
      );
      expect(
        router.route(path('bernd/-12')),
        const Res(#berndId, <String, dynamic>{'berndId': -12}),
      );
      expect(
        router.route(path('multipleParams/-9')),
        const Res(#multipleParamsError, emptyMap),
      );
      expect(
        router.route(path('multipleParams/9')),
        const Res(#multipleUintId, <String, int>{'multipleUintId': 9}),
      );
      expect(
        router.route(path('multipleParams/tes')),
        const Res(#regexParam, <String, String>{'regexParam': 'tes'}),
      );
      expect(
        router.route(path('multipleParams/test')),
        const Res(#multipleParamsError, emptyMap),
      );
    });

    group('#Duplicate params', () {
      test('Throws AssertionError on nested params with the same id', () {
        expect(
            () => Segment.root<Res>(
                  create: Res.builder(#root),
                  createError: Res.builder(#rootError),
                  children: [
                    Segment.param(
                        parser: const UintParser('id'),
                        create: Res.builder(#id),
                        children: [
                          Segment.param(
                            parser: const IntParser('id'),
                            create: Res.builder(#id),
                          )
                        ]),
                  ],
                ),
            throwsA(isA<ValidationError>()));
      });

      test(
          'Completes normally when there are multiple non conflicting params with the same id',
          () {
        expect(
          () => Segment.root<Res>(
            create: Res.builder(#root),
            createError: Res.builder(#rootError),
            children: [
              Segment.param(
                parser: const UintParser('id'),
                create: Res.builder(#id),
              ),
              Segment.param(
                parser: const UintParser('id'),
                create: Res.builder(#id),
              ),
            ],
          ),
          returnsNormally,
        );
      });
    });

    group('Returns the correct error', () {
      test(
          'Returns the error from the node that failed or the first node above the ones that failed',
          () {
        final router = Segment.root<Res>(
          create: Res.builder(#root),
          createError: Res.builder(#rootError),
          children: [
            Segment.path(
              name: 'user',
              create: Res.builder(#user),
            ),
            Segment.path(
              name: 'settings',
              create: Res.builder(#settings),
              createError: Res.builder(#settingsError),
              children: [
                Segment.path(
                  name: 'privacy',
                  create: Res.builder(#privacy),
                ),
              ],
            ),
          ],
        );

        expect(
          router.route(Uri.parse('https://router.test/nonExistingPath')),
          const Res(#rootError, emptyMap),
        );
        expect(
          router.route(Uri.parse('https://router.test/user/nonExistingPath')),
          const Res(#rootError, emptyMap),
        );
        expect(
          router.route(
              Uri.parse('https://router.test/settings/nonExistingSetting')),
          const Res(#settingsError, emptyMap),
        );
      });

      test('Throws AssertionError for duplicate PathSegments', () {
        expect(
          () => Segment.root<Res>(
            create: Res.builder(#root),
            createError: Res.builder(#rootError),
            children: [
              Segment.path(
                name: 'user',
                create: Res.builder(#user1),
              ),
              Segment.path(
                name: 'user',
                create: Res.builder(#user2),
              ),
            ],
          ),
          throwsA(isA<AssertionError>()),
        );
      });
    });

    test(
        'Lookup a LiteralParser before other parsers regardless of order of definition',
        () {
      final matchAny = RegExp(r'.*');
      final router = Segment.root<String>(
        create: (context) => 'rootRoute',
        createError: (context) => 'rootError',
        children: [
          Segment.regExpPath(
            regExp: matchAny,
            create: (context) => 'regexRoute',
          ),
          Segment.path(
            name: 'user',
            create: (context) => 'userRoute',
          ),
        ],
      );

      expect(matchAny.hasMatch('userRoute'), true);
      expect(router.route(Uri.parse('https://test.com/user')), 'userRoute');
    });
  });
}
