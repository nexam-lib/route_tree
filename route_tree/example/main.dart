import 'package:route_tree/route_tree.dart';

void main() {
  final router = Segment.root<String>(
    create: (context) => 'home',
    createError: (context) => 'Error!',
    children: [
      Segment.path(
        name: 'first_path',
        create: (context) => 'routed to first_path',
      ),
      Segment.path(
        name: 'second_path',
        create: (context) => 'routed to second_path',
        children: [
          Segment.param(
            parser: const UintParser('id'),
            create: (context) => 'routed to /second_path/${context['id']}',
          ),
        ],
      ),
    ],
  );

  assert(
      router.route(Uri.parse('/second_path/12')) == 'routed to second_path/12');
}
